//
//  ViewControllerHistory.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerHistory: UIViewController {

    @IBOutlet weak var imageLatar: UIImageView!
    @IBOutlet weak var judulHistory: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var latarLogo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true

        latarLogo.image = UIImage.init(named: "historyRemove")
        judulHistory.image = UIImage.init(named: "histori")
        imageLatar.image = UIImage.init(named: "backgroundCreate")
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    @objc func backButtonTapped() {
        let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
        
        self.navigationController?.pushViewController(viewControllerHome!, animated: true)
    }

}
