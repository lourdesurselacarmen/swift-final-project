//
//  GroupDataInListGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 15/12/22.
//

import Foundation
struct GroupDataInListGroup {
    let groupDataInListGroup = UserDefaults.standard
    
    func setGroupName(groupName: String) {
        groupDataInListGroup.set(groupName, forKey: "groupName")
    }
    
    func getGroupName() -> String {
        return groupDataInListGroup.string(forKey: "groupName") ?? ""
    }
    
    func setTotalMember(totalMember: Int) {
        groupDataInListGroup.set(totalMember, forKey: "totalMember")
    }
    
    func getTotalMember() -> Int {
        return groupDataInListGroup.integer(forKey: "totalMember")
    }
    
    func setCreatedDate(createdDate: String) {
        groupDataInListGroup.set(createdDate, forKey: "createdDate")
    }
    
    func getCreatedDate() -> String {
        return groupDataInListGroup.string(forKey: "createdDate") ?? ""
    }
    
    func setTotalMoney(totalMoney: Double) {
        groupDataInListGroup.set(totalMoney, forKey: "totalMoney")
    }
    
    func getTotalMoney() -> Double {
        return groupDataInListGroup.double(forKey: "totalMoney")
    }
    
    func setWallet(wallet: Double) {
        groupDataInListGroup.set(wallet, forKey: "wallet")
    }
    
    func getWallet() -> Double {
        return groupDataInListGroup.double(forKey: "wallet")
    }
    
    func setIdTransaction(idTransaction: Int) {
        groupDataInListGroup.set(idTransaction, forKey: "idTransaction")
    }
    
    func getIdTransaction() -> Int {
        return groupDataInListGroup.integer(forKey: "idTransaction")
    }
    
    func setIdGroupTransaction(idGroupTransaction: Int) {
        groupDataInListGroup.set(idGroupTransaction, forKey: "idGroupTransaction")
    }
    
    func getIdGroupTransaction() -> Int {
        return groupDataInListGroup.integer(forKey: "idGroupTransaction")
    }
    
    func setWinner(winner: String) {
        groupDataInListGroup.set(winner, forKey: "winner")
    }
    
    func getWinner() -> String {
        return groupDataInListGroup.string(forKey: "winner") ?? ""
    }
    
    func setWinnerPhone(winnerPhone: String) {
        groupDataInListGroup.set(winnerPhone, forKey: "winnerPhone")
    }
    
    func getWinnerPhone() -> String {
        return groupDataInListGroup.string(forKey: "winnerPhone") ?? ""
    }
}
