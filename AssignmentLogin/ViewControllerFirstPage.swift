//
//  ViewControllerFirstPage.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit

class ViewControllerFirstPage: UIViewController {

    @IBOutlet weak var latarFirstPage: UIImageView!
    @IBOutlet weak var nextPage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        latarFirstPage.image = UIImage.init(named: "firstPage")
        
        nextPage.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
    }
    
    @objc func nextTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
