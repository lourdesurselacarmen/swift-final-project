//
//  ViewControllerJoinGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerJoinGroup: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewListGroup: UITableView!
    @IBOutlet weak var imageLatar: UIImageView!
    @IBOutlet weak var judulJoinGrup: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var latarLogo: UIImageView!
    
    var arrListGroup: [[String: Any]] = []
    let userDefaultManager = UserDefaultManager()
    let groupData = GroupData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        latarLogo.image = UIImage.init(named: "joinRemove")
        judulJoinGrup.image = UIImage.init(named: "join")
        imageLatar.image = UIImage.init(named: "backgroundCreate")
        imageLatar.layer.cornerRadius = 30
        imageLatar.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        tableViewListGroup.delegate = self
        tableViewListGroup.dataSource = self
        
        getListGroup()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    func getListGroup() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/search-group", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as! [String : Any]
                    let data = itemObject["data"] as! [[String: Any]]
                    self.arrListGroup = data
                    print(self.arrListGroup)
                    
                    let status = itemObject["status"] as? Int
                    let success = itemObject["success"] as? Bool
                    let message = itemObject["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewListGroup.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let itemGrup = arrListGroup[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellJoinGroup
        
        cell1.iconGroup.image = UIImage.init(named: "groupIconList")
        cell1.iconNama.image = UIImage.init(named: "iconNamaGrup")
        cell1.iconMember.image = UIImage.init(named: "iconMemberGrup")
        cell1.iconTanggal.image = UIImage.init(named: "iconTanggalGrup")
        
        cell1.viewLabel.layer.cornerRadius = 12
        let namaGrupItem = itemGrup["groupName"] as! String
        cell1.namaGrup.text = namaGrupItem
        let jumlahAnggotaItem = itemGrup["totalMember"] as! Int
        cell1.jumlahAnggota.text = "\(String(describing: jumlahAnggotaItem)) Anggota"
        let tanggalTerbuatItem = itemGrup["createdDate"] as! String
        cell1.tanggalTerbuat.text = "Since \(tanggalTerbuatItem)"
        
        cell1.joinButtonTappedCallback = {
            let token = self.userDefaultManager.getAuthToken()
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            let URL = "http://localhost:8080/api/arisan-arisan-club/search-group/\(namaGrupItem)"
            
            AF.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        let itemObjectKirim = response.value as? [String : Any]
                        let status = itemObjectKirim?["status"] as? Int
                        let success = itemObjectKirim?["success"] as? Bool
                        
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            if success! {
                                DispatchQueue.main.async {
                                    let dataKirim = itemObjectKirim?["data"] as? [String: Any]
                                    let groupNameKirim = dataKirim?["groupName"] as? String
                                    let memberKirim = dataKirim?["totalMember"] as? Int
                                    let tanggalTerbuatKirim = dataKirim?["createdDate"] as? String
                                    print(dataKirim)
                                    
                                    self.groupData.setGroupName(groupName: groupNameKirim!)
                                    self.groupData.setTotalMember(totalMember: memberKirim!)
                                    self.groupData.setCreatedDate(createdDate: tanggalTerbuatItem)
                                    
                                    let viewControllerJoinGroupCheck = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerJoinGroupCheck") as? ViewControllerJoinGroupCheck
                                    
                                    self.navigationController?.pushViewController(viewControllerJoinGroupCheck!, animated: true)
                                }
                            }
                        }
                            
                        case .failure(let error):
                            let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            print(error)
                        }
                    })
                }
                              
                return cell1
            }
                              
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print(indexPath.section)
    print(indexPath.row)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 128
    }
                              
    @objc func backButtonTapped() {
    let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome

    self.navigationController?.pushViewController(viewControllerHome!, animated: true)
    }
}
