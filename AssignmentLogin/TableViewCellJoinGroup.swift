//
//  TableViewCellJoinGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 15/12/22.
//

import UIKit

class TableViewCellJoinGroup: UITableViewCell {

    @IBOutlet weak var iconNama: UIImageView!
    @IBOutlet weak var buttonJoin: UIButton!
    @IBOutlet weak var tanggalTerbuat: UILabel!
    @IBOutlet weak var jumlahAnggota: UILabel!
    @IBOutlet weak var namaGrup: UILabel!
    @IBOutlet weak var iconTanggal: UIImageView!
    @IBOutlet weak var iconMember: UIImageView!
    @IBOutlet weak var iconGroup: UIImageView!
    @IBOutlet weak var viewLabel: UIView!
    
    var joinButtonTappedCallback: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonJoin.addTarget(self, action: #selector(buttonJoinTapped), for: .touchUpInside)
    }
    
    @objc func buttonJoinTapped() {
        self.joinButtonTappedCallback?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
