//
//  TableViewCellMyGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 15/12/22.
//

import UIKit

class TableViewCellMyGroup: UITableViewCell {
    
    @IBOutlet weak var money: UILabel!
    @IBOutlet weak var tanggalTerbuat: UILabel!
    @IBOutlet weak var jumlahAnggota: UILabel!
    @IBOutlet weak var namaGrup: UILabel!
    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var iconMoney: UIImageView!
    @IBOutlet weak var iconTanggal: UIImageView!
    @IBOutlet weak var iconMember: UIImageView!
    @IBOutlet weak var iconNama: UIImageView!
    @IBOutlet weak var buttonGroup: UIButton!
    @IBOutlet weak var iconGroup: UIImageView!
    
    var buttonGroupTappedCallback: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonGroup.addTarget(self, action: #selector(buttonGroupTapped), for: .touchUpInside)
    }
    
    @objc func buttonGroupTapped() {
        self.buttonGroupTappedCallback?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
