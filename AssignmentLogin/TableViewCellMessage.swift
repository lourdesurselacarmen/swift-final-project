//
//  TableViewCellMessage.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 16/12/22.
//

import UIKit

class TableViewCellMessage: UITableViewCell {

//    @IBOutlet weak var textViewMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var hapusMessage: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    
    var hapusButtonTappedCallback: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        hapusMessage.addTarget(self, action: #selector(hapusMessageTapped), for: .touchUpInside)
    }
    
    @objc func hapusMessageTapped() {
        self.hapusButtonTappedCallback?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
