//
//  ViewController.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 21/11/22.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var keteranganUsername: UILabel!
    @IBOutlet weak var signIn: UIImageView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var passShowButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var keteranganPassword: UILabel!
    
    var username = ""
    var password = ""
    var token1 = ""
    var eyeClick = true
    let userDefaultManager = UserDefaultManager()
    let userData = UserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        labelUsername.text = "Username"
        labelPassword.text = "Password"
        registerLabel.text = "Belum punya akun?"
        
        signIn.image = UIImage.init(named: "signIn")
        imageLogo.image = UIImage.init(named: "logo")
        
        textFieldUsername.delegate = self
        textFieldPassword.delegate = self
        
        keteranganUsername.isHidden = true
        keteranganPassword.isHidden = true
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        loginButton.isHidden = false
        loginButton.isEnabled = false
        
        registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        isLoggedIn()
    }
    
    func isLoggedIn() {
        let token = userDefaultManager.getAuthToken()
        if token != "" {
            let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
            
            self.navigationController?.pushViewController(viewControllerHome!, animated: true)
        }
    }
    
    @IBAction func usernameRegex(_ sender: Any) {
        if let password = textFieldUsername.text {
            if let errorMessage = invalidUsername(password) {
                keteranganUsername.text = errorMessage
                keteranganUsername.isHidden = false
            } else {
                keteranganUsername.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidUsername(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if eyeClick {
            textFieldPassword.isSecureTextEntry = false
        } else {
            textFieldPassword.isSecureTextEntry = true
        }
        eyeClick = !eyeClick
    }
    
    @IBAction func passwordRegex(_ sender: Any) {
        if let password = textFieldPassword.text {
            if let errorMessage = invalidPassword(password) {
                keteranganPassword.text = errorMessage
                keteranganPassword.isHidden = false
            } else {
                keteranganPassword.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count < 8) {
            return "Password minimal 8 karakter"
        }
        if !containsDigit(value) {
            return "Password harus memiliki minimal 1 angka"
        }
        if !containsLowerCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        if !containsUpperCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regex = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regex = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if keteranganPassword.isHidden && keteranganUsername.isHidden {
            loginButton.isEnabled = true
        } else {
            loginButton.isEnabled = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
        // handling code
    }
    
    @objc func registerButtonTapped() {
        let viewControllerRegister = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerRegister") as? ViewControllerRegister
        
        self.navigationController?.pushViewController(viewControllerRegister!, animated: true)
    }
    
    @objc func loginButtonTapped() {
        let param = ["username": textFieldUsername.text, "password": textFieldPassword.text]
        
        AF.request("http://localhost:8080/arisan-arisan-club/login", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
            .responseJSON(completionHandler: { response in
                //kalau get dia url encoding karena pas encode si parameternya bakal kebawa di url, sedangkan klo di post pake json encoding
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [String: Any]
                    let username = data?["username"] as? String
                    let role = data?["role"] as? String
                    let token = data?["token"] as? String
                    print(token)
                    
                    if (token ?? "" != "") {
                        if (role == "user") {
                            DispatchQueue.main.async {
                                self.userDefaultManager.setAuthToken(token: token!)
                                self.userData.setUsername(username: username!)
                                
                                let alert = UIAlertController(title: "Selamat Datang", message: "Anda berhasil melakukan log in!", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                    let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
                                    
                                    self.navigationController?.pushViewController(viewControllerHome!, animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        } else {
                            let alert = UIAlertController(title: "Login anda gagal!", message: "Anda seorang admin, silahkan log in di website", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        let alert = UIAlertController(title: "Login anda gagal!", message: "Silahkan cek kembali username dan passwordmu. Jika kamu belum punya akun silahkan register terlebih dahulu", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                case .failure(let error):
                    let alert = UIAlertController(title: "Login anda gagal!", message: "Silahkan cek kembali username dan passwordmu. Jika kamu belum punya akun silahkan register terlebih dahulu", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    print(error)
                }
            })
    }
}

