//
//  ViewControllerGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 11/12/22.
//

import UIKit
import Alamofire

class ViewControllerGroup: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var kocokArisanButton: UIButton!
    @IBOutlet weak var transaksiArisanGroupButton: UIButton!
    @IBOutlet weak var transaksiArisanGroupImage: UIImageView!
    @IBOutlet weak var kocokArisanImage: UIImageView!
    @IBOutlet weak var tableViewTransaksiGrup: UITableView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var walletIndoLabel: UILabel!
    @IBOutlet weak var walletLabel: UILabel!
    @IBOutlet weak var totalMoneyLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var totalMemberLabel: UILabel!
    @IBOutlet weak var namaGrupLabel: UILabel!
    @IBOutlet weak var viewLatarBawah: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var viewLatar: UIView!
    
    var groupDataInListGroup = GroupDataInListGroup()
    var userDefaultManager = UserDefaultManager()
    
    var arrListTransaksiGroup: [[String: Any]] = []
    var wallet: Double = 0
    var data: [String: Any] = [:]
    var username = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        getGroupInfo()
        getCurrentUser()
        
        viewLatarBawah.layer.cornerRadius = 30
        viewLatarBawah.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        tableViewTransaksiGrup.layer.cornerRadius = 30
        tableViewTransaksiGrup.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        tableViewTransaksiGrup.delegate = self
        tableViewTransaksiGrup.dataSource = self
        
        namaGrupLabel.text = groupDataInListGroup.getGroupName()
        totalMemberLabel.text = "\(groupDataInListGroup.getTotalMember()) Anggota"
        createdDateLabel.text = "Since \(groupDataInListGroup.getCreatedDate())"
        let totalMoneyGetInt = Int(groupDataInListGroup.getTotalMoney())
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: totalMoneyGetInt as NSNumber) {
            totalMoneyLabel.text = "Biaya Arisan Rp " + formattedTipAmount + ",00"
        }
        walletIndoLabel.text = "Wallet Grup"
        
        kocokArisanImage.image = UIImage.init(named: "kocokArisan")
        kocokArisanImage.layer.cornerRadius = 12
        transaksiArisanGroupImage.image = UIImage.init(named: "bayarArisan")
        transaksiArisanGroupImage.layer.cornerRadius = 12
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
        transaksiArisanGroupButton.addTarget(self, action: #selector(transaksiArisanpButtonTapped), for: .touchUpInside)
        messageButton.addTarget(self, action: #selector(messageButtonTapped), for: .touchUpInside)
        kocokArisanButton.addTarget(self, action: #selector(self.kocokArisanButtonTapped), for: .touchUpInside)
        
        getTransaksiGrup()
    }
    
    func getCurrentUser() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/current-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    print(response.value)
                    let itemObject = response.value as? [String : Any]
                    let status = itemObject?["status"] as? Int
                    let data = itemObject?["data"] as? [String: Any]
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.username = data?["username"] as? String ?? ""
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func getGroupInfo() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/search-group/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    self.data = itemObject?["data"] as! [String: Any]
                    let groupWallet = self.data["groupWallet"] as! [String: Any]
                    self.wallet = groupWallet["groupWalletBalance"] as! Double
                    print("Wallet: \(self.wallet)")
                    
                    let walletInt = Int(self.wallet)
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "id_ID")
                    formatter.groupingSeparator = "."
                    formatter.numberStyle = .decimal
                    if let formattedTipAmount = formatter.string(from: walletInt as NSNumber) {
                        if (walletInt == 0) {
                            self.walletLabel.text = "Rp " + formattedTipAmount
                        } else {
                            self.walletLabel.text = "Rp " + formattedTipAmount + ",00"
                        }
                    }
                    
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    let message = itemObject?["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        let viewControllerMyGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerMyGroup") as? ViewControllerMyGroup
        
        self.navigationController?.pushViewController(viewControllerMyGroup!, animated: true)
    }
    
    @objc func infoButtonTapped() {
        let viewControllerGroupInfo = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroupInfo") as? ViewControllerGroupInfo
        
        self.navigationController?.pushViewController(viewControllerGroupInfo!, animated: true)
    }
    
    @objc func kocokArisanButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/cek-admin-grup/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as! [String : Any]
                    
                    let status = itemObject["status"] as? Int
                    let code = itemObject["code"] as? Int
                    let success = itemObject["success"] as? Bool
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        if (success!) {
                            let viewControllerKocokArisan = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerKocokArisan") as? ViewControllerKocokArisan
                            
                            self.navigationController?.pushViewController(viewControllerKocokArisan!, animated: true)
                        } else {
                            let alert = UIAlertController(title: "Hey!", message: "Kamu bukan seorang admin grup", preferredStyle: UIAlertController.Style.alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func messageButtonTapped() {
        let viewControllerGroupPost = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroupPost") as? ViewControllerGroupPost
        
        self.navigationController?.pushViewController(viewControllerGroupPost!, animated: true)
    }
    
    @objc func transaksiArisanpButtonTapped() {
        let viewControllerTransaksiUser = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerTransaksiUser") as? ViewControllerTransaksiUser
        
        self.navigationController?.pushViewController(viewControllerTransaksiUser!, animated: true)
    }
    
    func getTransaksiGrup() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/history-transaksi/group/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]]
                    
                    self.arrListTransaksiGroup = data ?? []
                    print(self.arrListTransaksiGroup)
                    
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    let message = itemObject?["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewTransaksiGrup.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListTransaksiGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemArrayListGrup = arrListTransaksiGroup[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellGroup
        
        cell1.viewLabel.layer.cornerRadius = 20
        cell1.viewLabel2.layer.cornerRadius = 20
        cell1.viewLabel2.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        cell1.dateIcon.image = UIImage.init(named: "dateOfGroupTransaction")
        cell1.moneyIcon.image = UIImage.init(named: "moneyGroupTransaction")
        cell1.winnerIcon.image = UIImage.init(named: "winnerGroupTransaction")
        cell1.statusIcon.image = UIImage.init(named: "statusGroupTransaction")
        cell1.descriptionIcon.image = UIImage.init(named: "descriptionGroupTransaction")
        
        let memberInvolvedItem = itemArrayListGrup["memberInvolved"] as! String
        cell1.memberInvolved.text = memberInvolvedItem
        let dateOfTransactionItem = itemArrayListGrup["dateOfGroupTransaction"] as! String
        cell1.dateOfTransaction.text = "Tanggal Transaksi: \( dateOfTransactionItem)"
        let moneyTransferedItem = itemArrayListGrup["moneyTransfered"] as! Double
        
        let moneyTransferedItemInt = Int(moneyTransferedItem)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: moneyTransferedItemInt as! NSNumber) {
            cell1.moneyTransfered.text = "Nominal: Rp " + formattedTipAmount + ",00"
        }
        
        let winnerItem = itemArrayListGrup["winner"] as! String
        cell1.winner.text = "Pemenang Arisan: \(winnerItem)"
        let statusTransferItem = itemArrayListGrup["transfered"] as! Bool
        if statusTransferItem {
            cell1.statusTransfer.text = "Status Transaksi: Sudah transfer"
        } else {
            cell1.statusTransfer.text = "Status Transaksi: Belum transfer"
        }
        
        cell1.deskripsiJudul.text = "Deskripsi Transaksi:"
        let deskripsiItem = itemArrayListGrup["descriptionActivity"] as! String
        cell1.deskripsi.text = deskripsiItem
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
}
