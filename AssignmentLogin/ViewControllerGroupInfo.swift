//
//  ViewControllerGroupInfo.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 11/12/22.
//

import UIKit
import Alamofire

class ViewControllerGroupInfo: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var labelMemberJudul: UILabel!
    @IBOutlet weak var tableViewMember: UITableView!
    @IBOutlet weak var changeGroupInfoButton: UIButton!
    @IBOutlet weak var textFieldPasscode: UITextField!
    @IBOutlet weak var textFieldNama: UITextField!
    @IBOutlet weak var passcode: UILabel!
    @IBOutlet weak var namaGrup: UILabel!
    @IBOutlet weak var labelJudul: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var groupDataInListGroup = GroupDataInListGroup()
    var userDefaultManager = UserDefaultManager()
    
    var arrListMember: [[String: Any]] = []
    var arrListWinner: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        textFieldNama.delegate = self
        textFieldPasscode.delegate = self
        
        labelJudul.text = "Info Grup"
        namaGrup.text = "Nama Grup:"
        passcode.text = "Passcode:"
        textFieldNama.text = groupDataInListGroup.getGroupName()
        labelMemberJudul.text = "Anggota Grup"
        
        tableViewMember.delegate = self
        tableViewMember.dataSource = self

        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        changeGroupInfoButton.addTarget(self, action: #selector(changeGroupInfoButtonTapped), for: .touchUpInside)
        
        getMemberGrup()
        getWinnerArisan()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
        
        self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
    }
    
    @objc func changeGroupInfoButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["groupName": textFieldNama.text, "passcode": textFieldPasscode.text]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/change-group-info/\(groupDataInListGroup.getGroupName())", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as! [String : Any]
                    print(itemObject)
                    
                    let status = itemObject["status"] as? Int
                    let success = itemObject["success"] as? Bool
                    let message = itemObject["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        
                        if success ?? false == true {
                            let alert = UIAlertController(title: "Berhasil!", message: "Anda telah mengubah info group ini", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewControllerMyGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerMyGroup") as? ViewControllerMyGroup
                                
                                self.navigationController?.pushViewController(viewControllerMyGroup!, animated: true)
                                
                                self.textFieldPasscode.text = ""
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            let alert = UIAlertController(title: "Gagal!", message: "Kamu bukan seorang admin.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func getWinnerArisan() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/history-transaksi/group/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]] ?? []
                    
                    for eachTransaction in data {
                        let winner = eachTransaction["winner"] as? String
                        self.arrListWinner.append(winner ?? "")
                    }
                    
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    let message = itemObject?["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func getMemberGrup() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/search-group-member/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as! [String : Any]
                    let data = itemObject["data"] as! [[String: Any]]
                    self.arrListMember = data
                    print(self.arrListMember)
                    
                    let status = itemObject["status"] as? Int
                    let success = itemObject["success"] as? Bool
                    let message = itemObject["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewMember.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListMember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemArrayListMember = arrListMember[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellMember
        
        cell1.viewLabel.layer.cornerRadius = 12
        cell1.viewLabel.layer.borderWidth = 0.5
        cell1.viewLabel.layer.borderColor = UIColor.black.cgColor
        cell1.imageProfil.image = UIImage.init(named: "profilPicture")
        
        let user = itemArrayListMember["user"] as! [String: Any]
        let memberFullnameItem = user["fullName"] as! String
        cell1.labelFullname.text = memberFullnameItem
        let memberUsernameItem = user["username"] as! String
        cell1.labelUsername.text = "Username: \(memberUsernameItem)"
        
        for eachWinner in arrListWinner {
            if (memberUsernameItem == eachWinner) {
                cell1.viewLabel.backgroundColor = UIColor(red: 233/255, green: 229/255, blue: 230/255, alpha: 1.0)
            }
        }
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
