//
//  TableViewCellTransaksi.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 19/12/22.
//

import UIKit

class TableViewCellTransaksi: UITableViewCell {

    @IBOutlet weak var deadline: UILabel!
    @IBOutlet weak var dendaFixed: UILabel!
    @IBOutlet weak var nama: UILabel!
    @IBOutlet weak var denda: UILabel!
    @IBOutlet weak var textFieldDenda: UITextField!
    @IBOutlet weak var bayarButton: UIButton!
    @IBOutlet weak var statusGroupTransaction: UILabel!
    @IBOutlet weak var dateOfGroupTransaction: UILabel!
    @IBOutlet weak var moneyTransfered: UILabel!
    @IBOutlet weak var descriptionActivity: UILabel!
    @IBOutlet weak var viewLabel: UIView!
    
    var bayarButtonTappedCallback: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bayarButton.addTarget(self, action: #selector(bayarButtonTapped), for: .touchUpInside)
    }
    
    @objc func bayarButtonTapped() {
        self.bayarButtonTappedCallback?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
