//
//  TableViewCellMember.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 16/12/22.
//

import UIKit

class TableViewCellMember: UITableViewCell {

    @IBOutlet weak var viewLabel: UIView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var imageProfil: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
