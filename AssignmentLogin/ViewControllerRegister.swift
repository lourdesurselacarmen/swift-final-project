//
//  ViewControllerRegister.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 05/12/22.
//

import UIKit
import Alamofire

class ViewControllerRegister: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var keteranganUsername: UILabel!
    @IBOutlet weak var keteranganFullname: UILabel!
    @IBOutlet weak var keteranganGender: UILabel!
    @IBOutlet weak var eyeButtonRepeatPass: UIButton!
    @IBOutlet weak var eyeButtonPass: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var registerImage: UIImageView!
    @IBOutlet weak var keteranganUlangPass: UILabel!
    @IBOutlet weak var keteranganPass: UILabel!
    @IBOutlet weak var keteranganTelp: UILabel!
    @IBOutlet weak var ulangPassText: UITextField!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var noTelptext: UITextField!
    @IBOutlet weak var genderText: UITextField!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var fullnameText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var ulangPassLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var noTelpLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    
    @IBOutlet weak var textFieldDOB: UITextField!
    
    var eyeClickPass = true
    var eyeClickRepeatPass = true
    var arrGender = ["Perempuan", "Laki-laki","Memilih tidak menjawab"]
    var pickerGender = UIPickerView()
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        fullnameLabel.text = "Nama Lengkap"
        usernameLabel.text = "Username"
        dobLabel.text = "Tanggal Lahir"
        genderLabel.text = "Jenis Kelamin"
        noTelpLabel.text = "Nomor Telepon"
        passwordLabel.text = "Password"
        ulangPassLabel.text = "Ulangi Password"
        loginLabel.text = "Sudah punya akun?"
        
        registerImage.image = UIImage.init(named: "Register")
        
        keteranganTelp.isHidden = true
        keteranganPass.isHidden = true
        keteranganUlangPass.isHidden = true
        keteranganGender.isHidden = true
        keteranganFullname.isHidden = true
        keteranganUsername.isHidden = true
        
        fullnameText.delegate = self
        usernameText.delegate = self
        textFieldDOB.delegate = self
        genderText.delegate = self
        noTelptext.delegate = self
        passText.delegate = self
        ulangPassText.delegate = self
        
        registerButton.addTarget(self, action: #selector(registerTapped), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        textFieldDOB.inputView = datePicker
        textFieldDOB.inputAccessoryView = toolbar
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.lightGray
        toolBar.sizeToFit()

        let doneButtonGender = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker))
        let spaceButtonGender = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButtonGender = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))

        toolBar.setItems([cancelButtonGender, spaceButtonGender, doneButtonGender], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerGender.delegate = self
        self.genderText.inputView = pickerGender
        self.genderText.inputAccessoryView = toolBar
        
        registerButton.isHidden = false
        registerButton.isEnabled = false
    }
    
    //picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        genderText.text = "\(arrGender[row])"
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        textFieldDOB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @objc func donePicker() {
        genderText.resignFirstResponder()
    }
    
    @IBAction func showPass(_ sender: Any) {
        if eyeClickPass {
            passText.isSecureTextEntry = false
        } else {
            passText.isSecureTextEntry = true
        }
        eyeClickPass = !eyeClickPass
    }
    
    @IBAction func showRepeatPass(_ sender: Any) {
        if eyeClickRepeatPass {
            ulangPassText.isSecureTextEntry = false
        } else {
            ulangPassText.isSecureTextEntry = true
        }
        eyeClickRepeatPass = !eyeClickRepeatPass
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        textFieldDOB.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    @objc func loginTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @objc func registerTapped() {
        let alert = UIAlertController(title: "Konfirmasi!", message: "Apakah datamu sudah benar untuk registrasi?", preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: "Ya", style: UIAlertAction.Style.default, handler: {action in
            let param = ["fullName": self.fullnameText.text!, "username": self.usernameText.text!, "dateOfBirth":self.textFieldDOB.text!, "gender":self.genderText.text!, "phoneNumber":self.noTelptext.text!, "password":self.passText.text!]
            
            AF.request("http://localhost:8080/arisan-arisan-club/register", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                        
                    case .success:
                        print(response.value)
                        let itemObject = response.value as? [String : Any]
                        let data = itemObject?["data"] as? [String: Any]
                        let success = itemObject?["success"] as? Bool
                        let message = itemObject?["message"] as? String
                        
                        if success! {
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Akun telah didaftarkan!", message: "Silahkan lakukan login dengan akun yang telah didaftarkan.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                    
                                    self.navigationController?.pushViewController(viewController!, animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        } else {
                            let alert = UIAlertController(title: "Akun gagal didaftarkan!", message: "Username atau nomor telepon yang kamu gunakan sudah pernah digunakan, silahkan ganti username atau nomor teleponmu.", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    case .failure(let error):
                        let alert = UIAlertController(title: "Akun gagal didaftarkan!", message: "Silahkan coba lagi untuk melakukan registrasi akun.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print(error)
                    }
                })
        })
        let cancel = UIAlertAction(title: "Tidak", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func fullname(_ sender: Any) {
        if let fullname = fullnameText.text {
            if let errorMessage = invalidFullname(fullname) {
                keteranganFullname.text = errorMessage
                keteranganFullname.isHidden = false
            } else {
                keteranganFullname.isHidden = true
            }
        }
        //        checkValidation()
    }
    
    func invalidFullname(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func username(_ sender: Any) {
        if let username = usernameText.text {
            if let errorMessage = invalidUsername(username) {
                keteranganUsername.text = errorMessage
                keteranganUsername.isHidden = false
            } else {
                keteranganUsername.isHidden = true
            }
        }
        //        checkValidation()
    }
    
    func invalidUsername(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func showGender(_ sender: Any) {
        if let repeatGender = genderText.text {
            if let errorMessage = invalidGender(repeatGender) {
                keteranganGender.text = errorMessage
                keteranganGender.isHidden = false
            } else {
                keteranganGender.isHidden = true
            }
        }
        //        checkValidation()
    }
    
    func invalidGender(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func cekPassword(_ sender: Any) {
        if let repeatPass = ulangPassText.text {
            if let errorMessage = invalidRepeatPass(repeatPass) {
                keteranganUlangPass.text = errorMessage
                keteranganUlangPass.isHidden = false
            } else {
                keteranganUlangPass.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidRepeatPass(_ value: String) -> String? {
        if !repeatPassword(value) {
            return "Password harus sama dengan ulangi password"
        }
        return nil
    }
    
    func repeatPassword(_ value: String) -> Bool {
        let passwordText = passText.text
        let repeatPass = ulangPassText.text
        if passwordText == repeatPass {
            return true
        }
        return false
    }
    
    @IBAction func telpRegex(_ sender: Any) {
        if let noTelp = noTelptext.text {
            if let errorMessage = invalidTelp(noTelp) {
                keteranganTelp.text = errorMessage
                keteranganTelp.isHidden = false
            } else {
                keteranganTelp.isHidden = true
            }
        }
        //        checkValidation()
    }
    
    func invalidTelp(_ value: String) -> String? {
        if (value.count < 10) {
            return "Nomor telepon minimal 10 angka"
        }
        if !containsDigit(value) {
            return "Nomor telepon hanya boleh angka"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
    
    @IBAction func passRegex(_ sender: Any) {
        if let password = passText.text, let ulangPass = ulangPassText.text {
            if let errorMessage = invalidPassword(password) {
                keteranganPass.text = errorMessage
                keteranganPass.isHidden = false
            } else {
                keteranganPass.isHidden = true
            }
        }
        //        checkValidation()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count < 8) {
            return "Password minimal 8 karakter"
        }
        if !containsDigitPass(value) {
            return "Password harus memiliki minimal 1 angka"
        }
        if !containsLowerCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        if !containsUpperCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        return nil
    }
    
    func containsDigitPass(_ value: String) -> Bool {
        let regex = ".*[0-8].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regex = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regex = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if keteranganTelp.isHidden && keteranganPass.isHidden && keteranganUlangPass.isHidden && keteranganGender.isHidden && keteranganUsername.isHidden && keteranganFullname.isHidden {
            registerButton.isEnabled = true
        } else {
            registerButton.isEnabled = false
        }
    }
    
}
