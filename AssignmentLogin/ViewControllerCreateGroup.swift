//
//  ViewControllerCreateGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerCreateGroup: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var keteranganNama: UILabel!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var keteranganPasscode: UILabel!
    @IBOutlet weak var imageLatar: UIImageView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var textFieldPasscode: UITextField!
    @IBOutlet weak var imagePasscode: UIImageView!
    @IBOutlet weak var textFieldTotalMoney: UITextField!
    @IBOutlet weak var imageTotalMoney: UIImageView!
    @IBOutlet weak var textFieldGroupName: UITextField!
    @IBOutlet weak var imageGroupName: UIImageView!
    @IBOutlet weak var judulBuatGrup: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var latarLogo: UIImageView!
    
    var arrArisanValue = [500000, 1000000, 2000000, 3000000]
    var arrArisan = ["Rp 500.000,00", "Rp 1.000.000,00", "Rp 2.000.000,00", "Rp 3.000.000,00"]
    var pickerArisan = UIPickerView()
    var eyeClickPass = true
    var totalMoney = 0
    
    var userDefaultManager = UserDefaultManager()
    var groupDataInListGroup = GroupDataInListGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        textFieldTotalMoney.delegate = self
        textFieldGroupName.delegate = self
        textFieldPasscode.delegate = self
        
        keteranganPasscode.isHidden = true
        keteranganNama.isHidden = true
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.lightGray
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.donePicker))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerArisan.delegate = self
        self.textFieldTotalMoney.inputView = pickerArisan
        self.textFieldTotalMoney.inputAccessoryView = toolBar
        
        self.navigationController?.navigationBar.isHidden = true
        
        latarLogo.image = UIImage.init(named: "buatRemove")
        judulBuatGrup.image = UIImage.init(named: "buat")
        imageGroupName.image = UIImage.init(named: "createGrup")
        imageTotalMoney.image = UIImage.init(named: "totalMoney")
        imagePasscode.image = UIImage.init(named: "passcode")
        imageLatar.image = UIImage.init(named: "backgroundCreate")
        imageLatar.layer.cornerRadius = 30
        imageLatar.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        createButton.addTarget(self, action: #selector(createButtonTapped), for: .touchUpInside)
        
        createButton.isHidden = false
        createButton.isEnabled = false
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @IBAction func showPasscode(_ sender: Any) {
        if eyeClickPass {
            textFieldPasscode.isSecureTextEntry = false
        } else {
            textFieldPasscode.isSecureTextEntry = true
        }
        eyeClickPass = !eyeClickPass
    }
    
    //picker view
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrArisan.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrArisan[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        totalMoney = arrArisanValue[row]
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: totalMoney as NSNumber) {
            textFieldTotalMoney.text = "Rp " + formattedTipAmount + ",00"
        }
    }
    
    @objc func donePicker() {
        textFieldTotalMoney.resignFirstResponder()
    }
    
    // regex
    
    @IBAction func regexNamaGrup(_ sender: Any) {
        if let namaGrup = textFieldGroupName.text {
            if let errorMessage = invalidNama(namaGrup) {
                keteranganNama.isHidden = false
                keteranganNama.text = errorMessage
            } else {
                keteranganNama.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidNama(_ value: String) -> String? {
        if (value.count > 15) {
            return "Nama grup maksimal 15 karakter"
        }
        return nil
    }
    
    @IBAction func regexPasscode(_ sender: Any) {
        if let repeatPass = textFieldPasscode.text {
            if let errorMessage = invalidPassword(repeatPass) {
                keteranganPasscode.isHidden = false
                keteranganPasscode.text = errorMessage
            } else {
                keteranganPasscode.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count < 8) {
            return "Password minimal 8 karakter"
        }
        if !containsDigitPass(value) {
            return "Password harus memiliki minimal 1 angka"
        }
        return nil
    }
    
    func containsDigitPass(_ value: String) -> Bool {
        let regex = ".*[0-8].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if keteranganPasscode.isHidden && keteranganNama.isHidden {
            createButton.isEnabled = true
        } else {
            createButton.isEnabled = false
        }
    }
    
    @objc func createButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["groupName": textFieldGroupName.text!, "totalMoney": totalMoney, "passcode":textFieldPasscode.text!] as [String : Any]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/create-group", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
            switch response.result {
        
            case .success:
                print(response.value)
                let itemObject = response.value as? [String : Any]
                let data = itemObject?["data"] as? [String: Any]
                let status = itemObject?["status"] as? Int
                let success = itemObject?["success"] as? Bool
                let message = itemObject?["message"] as? String
                
                if status ?? 0 == 401 {
                    UserDefaults.standard.removeObject(forKey: "token")
                    //logout

                    let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                        
                        self.navigationController?.pushViewController(viewController!, animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    
                    if success! {
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Berhasil!", message: "Grup anda berhasil didaftarkan. Silahkan cek pada menu grup saya", preferredStyle: UIAlertController.Style.alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                self.textFieldGroupName.text = ""
                                self.textFieldTotalMoney.text = ""
                                self.textFieldPasscode.text = ""
                            })
                            let bukaGrup = UIAlertAction(title: "Buka Grup", style: UIAlertAction.Style.default, handler: {action in
                                let groupNameKirim = data?["groupName"] as? String
                                let memberKirim = data?["totalMember"] as? Int
                                let tanggalTerbuatKirim = data?["createdDate"] as? String
                                let totalMoneyKirim = data?["totalMoney"] as? Double
                                let wallet = data?["groupWallet"] as? [String: Any]
                                let walletBalance = wallet?["groupWalletBalance"] as? Double
                                print(data)
                                
                                self.groupDataInListGroup.setGroupName(groupName: groupNameKirim!)
                                self.groupDataInListGroup.setTotalMember(totalMember: memberKirim!)
                                self.groupDataInListGroup.setCreatedDate(createdDate: tanggalTerbuatKirim!)
                                self.groupDataInListGroup.setTotalMoney(totalMoney: totalMoneyKirim!)
                                self.groupDataInListGroup.setWallet(wallet: walletBalance!)
                                
                                let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
                                self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
                            })
                            alert.addAction(ok)
                            alert.addAction(bukaGrup)
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        let alert = UIAlertController(title: "Gagal!", message: "Nama grup yang kamu daftarkan telah ada sebelumnya.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
        
            case .failure(let error):
                let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            print(error)
            }
        })
    }
    
    @objc func backButtonTapped() {
        let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
        
        self.navigationController?.pushViewController(viewControllerHome!, animated: true)
    }
}
