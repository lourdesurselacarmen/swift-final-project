//
//  ViewControllerProfil.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerProfil: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var keteranganTanggalLahir: UILabel!
    @IBOutlet weak var keteranganPass: UILabel!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var judulImage: UIImageView!
    @IBOutlet weak var logOutButoon: UIButton!
    @IBOutlet weak var keteranganGender: UILabel!
    @IBOutlet weak var keteranganNoTelp: UILabel!
    @IBOutlet weak var keteranganUsername: UILabel!
    @IBOutlet weak var keteranganFullname: UILabel!
    @IBOutlet weak var textFieldDIB: UITextField!
    @IBOutlet weak var textFieldGender: UITextField!
    @IBOutlet weak var textFieldNoTelp: UITextField!
    @IBOutlet weak var textFieldUsername: UITextField!
    @IBOutlet weak var textFieldFullname: UITextField!
    @IBOutlet weak var profilImage: UIImageView!
    @IBOutlet weak var backgroundProfil: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    let userData = UserData()
    let userDefaultManager = UserDefaultManager()
    
    var arrGender = ["Perempuan", "Laki-laki","Memilih tidak menjawab"]
    var pickerGender = UIPickerView()
    var datePicker = UIDatePicker()
    
    var eyeClickPass = true
    var eyeClickRepeatPass = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        backgroundProfil.image = UIImage.init(named: "backgroundProfil")
        profilImage.image = UIImage.init(named: "profil")
        judulImage.image = UIImage.init(named: "judulProfil")
        
        pickerGender.delegate = self
        self.textFieldGender.inputView = pickerGender
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        saveButton.isEnabled = false
        
        logOutButoon.addTarget(self, action: #selector(logOutButoonTapped), for: UIControl.Event.touchUpInside)
        
        textFieldFullname.delegate = self
        textFieldUsername.delegate = self
        textFieldNoTelp.delegate = self
        textFieldGender.delegate = self
        textFieldDIB.delegate = self
        textFieldPassword.delegate = self
        
        textFieldUsername.isEnabled = false
        
        textFieldFullname.text = userData.getFullname()
        textFieldUsername.text = userData.getUsername()
        textFieldGender.text = userData.getGender()
        textFieldNoTelp.text = userData.getNoTelp()
        textFieldDIB.text = userData.getBirthOfDate()
        textFieldPassword.text = ""
        
        keteranganFullname.isHidden = true
        keteranganUsername.isHidden = true
        keteranganGender.isHidden = true
        keteranganNoTelp.isHidden = true
        keteranganPass.isHidden = true
        keteranganTanggalLahir.isHidden = true
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        textFieldDIB.inputView = datePicker
        textFieldDIB.inputAccessoryView = toolbar
        
        checkValidation()
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        textFieldDIB.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return arrGender.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        textFieldGender.text = "\(arrGender[row])"
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        textFieldDIB.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @objc func logOutButoonTapped() {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertController.Style.actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: UIFont(name: "Kailasa-Bold", size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.black]
        let titleString = NSAttributedString(string: "Konfirmasi!", attributes: titleAttributes)
        let messageAttributes = [NSAttributedString.Key.font: UIFont(name: "Kailasa", size: 15)!, NSAttributedString.Key.foregroundColor: UIColor.black]
        let messageString = NSAttributedString(string: "Apakah kamu yakin ingin log out?", attributes: messageAttributes)
        alert.setValue(titleString, forKey: "attributedTitle")
        alert.setValue(messageString, forKey: "attributedMessage")
        
        let ok = UIAlertAction(title: "Ya", style: UIAlertAction.Style.default, handler: {action in
            UserDefaults.standard.removeObject(forKey: "token")
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            
            self.navigationController?.pushViewController(viewController!, animated: true)
        })
        let cancel = UIAlertAction(title: "Tidak", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
        
        self.navigationController?.pushViewController(viewControllerHome!, animated: true)
    }
    
    @objc func saveButtonTapped() {
        let alert = UIAlertController(title: "Konfirmasi!", message: "Apakah kamu yakin ingin mengubah profilmu?", preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: "Lanjutkan", style: UIAlertAction.Style.default, handler: {action in
            let token = self.userDefaultManager.getAuthToken()
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            let param = ["fullName": self.textFieldFullname.text, "username": self.textFieldUsername.text, "phoneNumber": self.textFieldNoTelp.text, "gender": self.textFieldGender.text, "dateOfBirth": self.textFieldDIB.text, "password": self.textFieldPassword.text]
            
            AF.request("http://localhost:8080/api/arisan-arisan-club/userdata/ganti-profil", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        print(response.value)
                        let itemObject = response.value as? [String : Any]
                        
                        let status = itemObject?["status"] as? Int
                        let success = itemObject?["success"] as? Bool
                        let message = itemObject?["message"] as? String
                        
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            //logout
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            if success! {
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "Berhasil", message: "Profil anda berhasil terganti", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                        if (self.textFieldPassword.text != "") {
                                            UserDefaults.standard.removeObject(forKey: "token")
                                            
                                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                            
                                            self.navigationController?.pushViewController(viewController!, animated: true)
                                        } else {
                                            let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
                                            
                                            self.navigationController?.pushViewController(viewControllerHome!, animated: true)
                                        }
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            } else {
                                let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    case .failure(let error):
                        let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print(error)
                    }
                }
                )
        })
        let cancel = UIAlertAction(title: "Batalkan", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(cancel)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fullnameSyarat(_ sender: Any) {
        if let fullname = textFieldFullname.text {
            if let errorMessage = invalidFullname(fullname) {
                keteranganFullname.text = errorMessage
                keteranganFullname.isHidden = false
            } else {
                keteranganFullname.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidFullname(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func usernameSyarat(_ sender: Any) {
        if let username = textFieldUsername.text {
            if let errorMessage = invalidUsername(username) {
                keteranganUsername.text = errorMessage
                keteranganUsername.isHidden = false
            } else {
                keteranganUsername.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidUsername(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func noTelpSyarat(_ sender: Any) {
        if let noTelp = textFieldNoTelp.text {
            if let errorMessage = invalidTelp(noTelp) {
                keteranganNoTelp.text = errorMessage
                keteranganNoTelp.isHidden = false
            } else {
                keteranganNoTelp.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidTelp(_ value: String) -> String? {
        if (value.count < 10) {
            return "Nomor telepon minimal 10 angka"
        }
        if !containsDigit(value) {
            return "Nomor telepon hanya boleh angka"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
    
    @IBAction func genderSyarat(_ sender: Any) {
        if let repeatGender = textFieldGender.text {
            if let errorMessage = invalidGender(repeatGender) {
                keteranganGender.text = errorMessage
                keteranganGender.isHidden = false
            } else {
                keteranganGender.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidGender(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    
    @IBAction func tanggalLahirSyarat(_ sender: Any) {
        if let tanggalLahir = textFieldDIB.text {
            if let errorMessage = invalidTanggalLahir(tanggalLahir) {
                keteranganTanggalLahir.text = errorMessage
                keteranganTanggalLahir.isHidden = false
            } else {
                keteranganTanggalLahir.isHidden = true
            }
        }
    }
    
    func invalidTanggalLahir(_ value: String) -> String? {
        if (value.count < 1) {
            return "Kolom ini harus diisi"
        }
        return nil
    }
    
    @IBAction func showPassword(_ sender: Any) {
        if eyeClickPass {
            textFieldPassword.isSecureTextEntry = false
        } else {
            textFieldPassword.isSecureTextEntry = true
        }
        eyeClickPass = !eyeClickPass
    }
    
    @IBAction func passwordSyarat(_ sender: Any) {
        if let password = textFieldPassword.text {
            if let errorMessage = invalidPassword(password) {
                keteranganPass.text = errorMessage
                keteranganPass.isHidden = false
            } else {
                keteranganPass.isHidden = true
            }
        }
        checkValidation()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count < 8) {
            return "Password minimal 8 karakter"
        }
        if !containsDigitPass(value) {
            return "Password harus memiliki minimal 1 angka"
        }
        if !containsLowerCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        if !containsUpperCase(value) {
            return "Password harus terdiri atas huruf besar dan huruf kecil"
        }
        return nil
    }
    
    func containsDigitPass(_ value: String) -> Bool {
        let regex = ".*[0-8].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool {
        let regex = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsUpperCase(_ value: String) -> Bool {
        let regex = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidation() {
        if keteranganFullname.isHidden && keteranganUsername.isHidden && keteranganNoTelp.isHidden && keteranganGender.isHidden && keteranganPass.isHidden {
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
    }
}
