//
//  ViewControllerTransaksiUser.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 11/12/22.
//

import UIKit
import Alamofire

class ViewControllerTransaksiUser: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var textFieldDeadline: UITextField!
    @IBOutlet weak var deadlineTransaksi: UILabel!
    @IBOutlet weak var tableViewTransaksi: UITableView!
    @IBOutlet weak var tambahTransaksi: UIButton!
    @IBOutlet weak var labelJudul: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var groupDataInListGroup = GroupDataInListGroup()
    var userDefaultManager = UserDefaultManager()
    var userData = UserData()
    
    var arrTransaksi: [[String: Any]] = []
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        labelJudul.text = "Transaksi"
        deadlineTransaksi.text = "Deadline Arisan"
        
        tableViewTransaksi.delegate = self
        tableViewTransaksi.dataSource = self
        
        textFieldDeadline.delegate = self
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        tambahTransaksi.addTarget(self, action: #selector(tambahTransaksiTapped), for: .touchUpInside)
        
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        textFieldDeadline.inputView = datePicker
        textFieldDeadline.inputAccessoryView = toolbar
        
        getTransaksi()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        textFieldDeadline.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    @objc func backButtonTapped() {
        let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
        
        self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        textFieldDeadline.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func getTransaksi() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/history-transaksi/group/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]]
                    self.arrTransaksi = data ?? []
                    print(self.arrTransaksi)
                    
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    let message = itemObject?["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewTransaksi.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func tambahTransaksiTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["paymentDeadline": textFieldDeadline.text]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/payment-arisan-data/\(groupDataInListGroup.getGroupName())", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObjectPlusTransaksi = response.value as? [String : Any]
                    let dataPlusTransaksi = itemObjectPlusTransaksi?["data"] as? [String: Any]
                    print(dataPlusTransaksi)
                    
                    let status = itemObjectPlusTransaksi?["status"] as? Int
                    let success = itemObjectPlusTransaksi?["success"] as? Bool
                    let message = itemObjectPlusTransaksi?["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewTransaksi.reloadData()
                    self.getTransaksi()
                    self.textFieldDeadline.text = ""
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTransaksi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemTransaksi = arrTransaksi[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellTransaksi
        
        cell1.bayarButton.isHidden = true
        cell1.bayarButton.isEnabled = true
        cell1.dendaFixed.isHidden = true
        cell1.textFieldDenda.isEnabled = false
        cell1.textFieldDenda.isHidden = false
        
        cell1.viewLabel.layer.cornerRadius = 12
        cell1.viewLabel.layer.borderWidth = 0.5
        cell1.viewLabel.layer.borderColor = UIColor.black.cgColor
        
        cell1.bayarButton.clipsToBounds = true
        cell1.bayarButton.layer.cornerRadius = 12
        cell1.bayarButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        let idTransactionItemGroup = itemTransaksi["idGroupTransaction"] as! Int
        
        let namaMemberItem = itemTransaksi["memberInvolved"] as! String
        cell1.nama.text = "Nama: \(namaMemberItem)"
        
        let descriptionActivityItem = itemTransaksi["descriptionActivity"] as! String
        cell1.descriptionActivity.text = "Aktivitas: \(descriptionActivityItem)"
        
        let moneyTransferedItem = itemTransaksi["moneyTransfered"] as! Double
        let moneyTransferedItemInt = Int(moneyTransferedItem)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: moneyTransferedItemInt as NSNumber) {
            cell1.moneyTransfered.text = "Jumlah transaksi: Rp " + formattedTipAmount + ",00"
        }
        
        let dateOfGroupTransactionItem = itemTransaksi["dateOfGroupTransaction"] as! String
        cell1.dateOfGroupTransaction.text = "Tanggal transaksi: \(dateOfGroupTransactionItem)"
        
        let statusTransaksiItem = itemTransaksi["transfered"] as! Bool
        if (statusTransaksiItem) {
            cell1.statusGroupTransaction.text = "Status transaksi: Sudah selesai"
        } else {
            cell1.statusGroupTransaction.text = "Status transaksi: Belum selesai"
        }
        
        cell1.denda.text = "Denda:"
        
        if (namaMemberItem == userData.getUsername()) {
            cell1.bayarButton.isHidden = false
            cell1.textFieldDenda.isEnabled = true
        }
        
        let transaksiItem = itemTransaksi["transaction"] as! [String: Any]
        let idTransaksi = transaksiItem["idTransaction"] as? Int
        let denda = transaksiItem["denda"] as? Double ?? 0
        let deadlinePayment = transaksiItem["paymentDeadline"] as? String ?? ""
        
        cell1.deadline.text = "Deadline Pembayaran: \(deadlinePayment)"
        
        let dendaInt = Int(denda)
        
        if (statusTransaksiItem) {
            if (denda == 0) {
                cell1.dendaFixed.text = "Rp 0"
            } else {
                if let formattedTipAmount = formatter.string(from: dendaInt as! NSNumber) {
                    cell1.dendaFixed.text = "Rp " + formattedTipAmount + ",00"
                }
            }
            cell1.bayarButton.isHidden = true
            cell1.textFieldDenda.isHidden = true
            cell1.dendaFixed.isHidden = false
            cell1.viewLabel.backgroundColor = UIColor(red: 247/255, green: 252/255, blue: 232/255, alpha: 1.0)
        } else {
            if let formattedTipAmount = formatter.string(from: dendaInt as! NSNumber) {
                cell1.dendaFixed.text = "Rp " + formattedTipAmount + ",00"
                cell1.viewLabel.backgroundColor = UIColor(red: 244/255, green: 227/255, blue: 255/255, alpha: 0.8)
            }
        }
        
        if (cell1.descriptionActivity.text == "Sudah transfer ke pemenang") {
            cell1.dendaFixed.isHidden = true
            cell1.denda.isHidden = true
            cell1.textFieldDenda.isHidden = true
        }
        
        cell1.bayarButtonTappedCallback = {
            let alert = UIAlertController(title: "Konfirmasi!", message: "Apakah anda yakin ingin membayar transaksi ini?", preferredStyle: UIAlertController.Style.alert)
            let ok = UIAlertAction(title: "Lanjutkan", style: UIAlertAction.Style.default, handler: {action in
                let token = self.userDefaultManager.getAuthToken()
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
                let param = ["denda": cell1.textFieldDenda.text]
                let URL = "http://localhost:8080/api/arisan-arisan-club/paymentCheck-arisan-data/\(self.groupDataInListGroup.getGroupName())/\(idTransactionItemGroup)"
                
                AF.request(URL, method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                            
                        case .success:
                            let itemObjectBayar = response.value as? [String : Any]
                            let dataBayar = itemObjectBayar?["data"] as? [[String: Any]]
                            print(dataBayar)
                            
                            let status = itemObjectBayar?["status"] as? Int
                            let success = itemObjectBayar?["success"] as? Bool
                            let message = itemObjectBayar?["message"] as? String
                            
                            if status ?? 0 == 401 {
                                UserDefaults.standard.removeObject(forKey: "token")
                                //logout
                                
                                let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                    
                                    self.navigationController?.pushViewController(viewController!, animated: true)
                                    
                                    self.getTransaksi()
                                    self.tableViewTransaksi.reloadData()
                                }))
                                self.present(alert, animated: true, completion: nil)
                            } else {
                                if (success ?? false == true) {
                                    let alert = UIAlertController(title: "Berhasil!", message: "Kamu sudah membayar transaksi ini", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.getTransaksi()
                                    self.tableViewTransaksi.reloadData()
                                } else {
                                    let alert = UIAlertController(title: "Gagal!", message: "Saldo wallet kamu tidak mencukupi", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    self.getTransaksi()
                                    self.tableViewTransaksi.reloadData()
                                }
                            }
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
            })
            let cancel = UIAlertAction(title: "Batalkan", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(cancel)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
}
