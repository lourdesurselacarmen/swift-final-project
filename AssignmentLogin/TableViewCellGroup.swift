//
//  TableViewCellGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 16/12/22.
//

import UIKit

class TableViewCellGroup: UITableViewCell {

    @IBOutlet weak var descriptionIcon: UIImageView!
    @IBOutlet weak var winnerIcon: UIImageView!
    @IBOutlet weak var moneyIcon: UIImageView!
    @IBOutlet weak var dateIcon: UIImageView!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var viewLabel2: UIView!
    @IBOutlet weak var deskripsi: UILabel!
    @IBOutlet weak var deskripsiJudul: UILabel!
    @IBOutlet weak var statusTransfer: UILabel!
    @IBOutlet weak var winner: UILabel!
    @IBOutlet weak var moneyTransfered: UILabel!
    @IBOutlet weak var dateOfTransaction: UILabel!
    @IBOutlet weak var memberInvolved: UILabel!
    @IBOutlet weak var viewLabel: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
