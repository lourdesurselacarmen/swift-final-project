//
//  UserDefaultManager.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import Foundation
struct UserDefaultManager {
    let userDefault = UserDefaults.standard
    
    func setAuthToken(token: String) {
        userDefault.set(token, forKey: "token")
    }
    
    func getAuthToken() -> String {
        return userDefault.string(forKey: "token") ?? ""
    }
}
