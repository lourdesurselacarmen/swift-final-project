//
//  ViewControllerGroupPost.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 11/12/22.
//

import UIKit
import Alamofire

class ViewControllerGroupPost: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableViewMessage: UITableView!
    @IBOutlet weak var viewConstrainMessage: NSLayoutConstraint!
    @IBOutlet weak var messageInput: UITextView!
    @IBOutlet weak var labelJudul: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var groupDataInListGroup = GroupDataInListGroup()
    var userDefaultManager = UserDefaultManager()
    
    var arrMessage: [[String: Any]] = []
    var messageItem = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        messageInput.delegate = self
        
        labelJudul.text = groupDataInListGroup.getGroupName()
        messageInput.isScrollEnabled = true
        messageInput.text = "Ketik Pesanmu Di Sini"
        messageInput.textColor = UIColor.lightGray
        messageInput.sizeToFit()
        messageInput.layer.borderWidth = 0.5
        messageInput.layer.borderColor = UIColor.black.cgColor
        messageInput.layer.cornerRadius = 10
        viewConstrainMessage.constant = self.messageInput.contentSize.height

        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(sendButtonTapped), for: .touchUpInside)
        
        tableViewMessage.delegate = self
        tableViewMessage.dataSource = self
        
        getMessage()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if messageInput.textColor == UIColor.lightGray {
            messageInput.text = nil
            messageInput.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if messageInput.text.isEmpty {
            messageInput.text = "Ketik Pesanmu Di Sini"
            messageInput.textColor = UIColor.lightGray
        }
    }
    
    @objc func backButtonTapped() {
        let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
        
        self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
    }
    
    @objc func sendButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["message": messageInput.text]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/post-message/group-board/\(groupDataInListGroup.getGroupName())", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]]
                    print(data)
                        
                        let status = itemObject?["status"] as? Int
                        let success = itemObject?["success"] as? Bool
                        let message = itemObject?["message"] as? String
                        
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            //logout
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    self.tableViewMessage.reloadData()
                    self.getMessage()
                    self.messageInput.text = ""
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func getMessage() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/group-board/\(groupDataInListGroup.getGroupName())", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]]
                
                    self.arrMessage = data ?? []
                        print(self.arrMessage)
                        
                        let status = itemObject?["status"] as? Int
                        let success = itemObject?["success"] as? Bool
                        let message = itemObject?["message"] as? String
                        
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            //logout
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.tableViewMessage.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemMessage = arrMessage[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellMessage
        
        let idMessage = itemMessage["idMessage"] as! Int
        let memberInvolvedItem = itemMessage["memberName"] as! String
        cell1.usernameLabel.text = memberInvolvedItem
        self.messageItem = itemMessage["message"] as! String
        cell1.textViewMessage.text = messageItem
        cell1.textViewMessage.isScrollEnabled = false
        cell1.textViewMessage.isEditable = false
        cell1.textViewMessage.layer.borderWidth = 0.5
        cell1.textViewMessage.layer.borderColor = UIColor.black.cgColor
        cell1.textViewMessage.layer.cornerRadius = 10
        cell1.textViewMessage.sizeToFit()
        cell1.textViewMessage.layoutIfNeeded()
        
        cell1.hapusButtonTappedCallback = {
            let token = self.userDefaultManager.getAuthToken()
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            AF.request("http://localhost:8080/api/arisan-arisan-club/delete-message/group-board/\(self.groupDataInListGroup.getGroupName())/\(idMessage)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                        
                    case .success:
                        let itemObject = response.value as? [String : Any]
                        let data = itemObject?["data"] as? [[String: Any]]
                            
                            let status = itemObject?["status"] as? Int
                            let success = itemObject?["success"] as? Bool
                            let message = itemObject?["message"] as? String
                            
                            if status ?? 0 == 401 {
                                UserDefaults.standard.removeObject(forKey: "token")
                                //logout
                                
                                let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                    
                                    self.navigationController?.pushViewController(viewController!, animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            } else {
                                if (success!) {
                                    let alert = UIAlertController(title: "Berhasil!", message: "Pesan ini berhasil dihapus", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    print(data)
                                } else {
                                    let alert = UIAlertController(title: "Gagal!", message: "Anda tidak dapat menghapus pesan orang lain", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        self.tableViewMessage.reloadData()
                        self.getMessage()
                        
                    case .failure(let error):
                        print(error)
                    }
                })
        }
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.messageItem.height(withConstrainedWidth: UIScreen.main.bounds.size.width - 80, font: UIFont.systemFont(ofSize: 15)) + 97
    }
}
