//
//  UserData.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import Foundation
struct UserData {
    let userData = UserDefaults.standard
    
    func setUsername(username: String) {
        userData.set(username, forKey: "username")
    }
    
    func getUsername() -> String {
        return userData.string(forKey: "username") ?? ""
    }
    
    func setFullname(fullName: String) {
        userData.set(fullName, forKey: "fullName")
    }
    
    func getFullname() -> String {
        return userData.string(forKey: "fullName") ?? ""
    }
    
    func setNoTelp(phoneNumber: String) {
        userData.set(phoneNumber, forKey: "phoneNumber")
    }
    
    func getNoTelp() -> String {
        return userData.string(forKey: "phoneNumber") ?? ""
    }
    
    func setGender(gender: String) {
        userData.set(gender, forKey: "gender")
    }
    
    func getGender() -> String {
        return userData.string(forKey: "gender") ?? ""
    }
    
    func setBirthOfDate(birthOfDate: String) {
        userData.set(birthOfDate, forKey: "birthOfDate")
    }
    
    func getBirthOfDate() -> String {
        return userData.string(forKey: "birthOfDate") ?? ""
    }
}
