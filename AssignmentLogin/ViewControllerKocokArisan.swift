//
//  ViewControllerKocokArisan.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 20/12/22.
//

import UIKit
import Alamofire

class ViewControllerKocokArisan: UIViewController {
    
    @IBOutlet weak var kocokArisan: UIButton!
    @IBOutlet weak var transferButton: UIButton!
    @IBOutlet weak var winnerLabel: UILabel!
    @IBOutlet weak var imageArisan: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var userDefaultManager = UserDefaultManager()
    var groupDataInListGroup = GroupDataInListGroup()
    
    var memberListInGroup: [String] = []
    var phoneNumber: String = ""
    var winnerRandom: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        transferButton.addTarget(self, action: #selector(transferButtonTapped), for: .touchUpInside)
        kocokArisan.addTarget(self, action: #selector(kocokArisanTapped), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        winnerLabel.isHidden = true
        
        imageArisan.image = UIImage.init(named: "arisanKocokProgress")
    }
    
    @objc func kocokArisanTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/kocok-arisan/\(groupDataInListGroup.getGroupName())", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    print(response.value)
                    let itemObject = response.value as! [String : Any]
                    
                    let status = itemObject["status"] as? Int
                    let success = itemObject["success"] as? Bool
                    let message = itemObject["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        if (success!) {
                            let data = itemObject["data"] as! [String: Any]
                            let groupTransaction = data["idGroupTransaction"] as! Int
                            self.groupDataInListGroup.setIdTransaction(idTransaction: groupTransaction)
                            
                            self.backButton.isEnabled = false
                            self.kocokArisan.isEnabled = false
                            self.winnerRandom = data["winner"] as! String
                            self.winnerLabel.isHidden = false
                            self.winnerLabel.text = "Pemenang: \(self.winnerRandom)"
                            self.imageArisan.image = UIImage.init(named: "arisanKocok")
                            self.getUser()
                            
                            let alert = UIAlertController(title: "Selamat!", message: "Pemenang arisan bulan ini telah ditentukan!", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            let alert = UIAlertController(title: "Gagal!", message: "Kocok ulang atau seluruh member sudah memperoleh giliran menang", preferredStyle: UIAlertController.Style.alert)
                            let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali", preferredStyle: UIAlertController.Style.alert)
                    let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    print(error)
                }
            })
    }
    
    func getUser() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/userdata/\(winnerRandom)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    print(response.value)
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [String: Any]
                    self.phoneNumber = data?["phoneNumber"] as? String ?? ""
                    self.groupDataInListGroup.setWinnerPhone(winnerPhone: self.phoneNumber)
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func transferButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let URLtransfer = "http://localhost:8080/api/arisan-arisan-club/kocok-arisan-pay/\(groupDataInListGroup.getGroupName())/\(groupDataInListGroup.getIdTransaction())/\(groupDataInListGroup.getWinnerPhone())"
        
        print(URLtransfer)
        
        AF.request(URLtransfer, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    print(response.value)
                    let itemObject = response.value as? [String : Any]
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        if (success!) {
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Berhasil!", message: "Transfer berhasil dilakukan", preferredStyle: UIAlertController.Style.alert)
                                
                                let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
                                alert.addAction(ok)
                                self.present(alert, animated: true, completion: nil)
                                self.backButton.isEnabled = true
                                self.transferButton.isEnabled = false
                            }
                            
                        } else {
                            let alert = UIAlertController(title: "Gagal!", message: "Transfer gagal, silahkan coba lagi", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
        
        self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
    }
}
