//
//  ViewControllerJoinGroupCheck.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 13/12/22.
//

import UIKit
import Alamofire

class ViewControllerJoinGroupCheck: UIViewController {
    
    @IBOutlet weak var masukButton: UIButton!
    @IBOutlet weak var textFieldPasscode: UITextField!
    @IBOutlet weak var showPass: UIButton!
    @IBOutlet weak var labelPasscode: UILabel!
    @IBOutlet weak var labelTanggal: UILabel!
    @IBOutlet weak var labelAnggota: UILabel!
    @IBOutlet weak var labelNamaGrup: UILabel!
    @IBOutlet weak var imageTanggal: UIImageView!
    @IBOutlet weak var imageAnggota: UIImageView!
    @IBOutlet weak var imageNamaGrup: UIImageView!
    @IBOutlet weak var imageGroup: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    let userDefaultManager = UserDefaultManager()
    let groupData = GroupData()
    let groupDataInListGroup = GroupDataInListGroup()
    var eyeClickPass = true
    var eyeClickRepeatPass = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        imageGroup.image = UIImage.init(named: "iconGroup")
        imageNamaGrup.image = UIImage.init(named: "iconNamaGrup2")
        imageAnggota.image = UIImage.init(named: "iconMemberGrup2")
        imageTanggal.image = UIImage.init(named: "iconTanggalGrup2")
        
        labelPasscode.text = "Silahkan masukan passcode grup"
        
        labelNamaGrup.text = groupData.getGroupName()
        labelAnggota.text = "\(groupData.getTotalMember()) Anggota"
        labelTanggal.text = "Since \(groupData.getCreatedDate())"
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        masukButton.addTarget(self, action: #selector(masukButtonTapped), for: .touchUpInside)
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func masukButtonTapped() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let param = ["passcode": textFieldPasscode.text]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/join-group/\(groupData.getGroupName())", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    print(response.value)
                    let itemObject = response.value as? [String : Any]
                    let status = itemObject?["status"] as? Int
                    let success = itemObject?["success"] as? Bool
                    
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            
                            if success ?? false == true {
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "Berhasil!", message: "Anda berhasil masuk grup ini", preferredStyle: UIAlertController.Style.alert)
                                    
                                    let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                        let viewControllerJoinGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerJoinGroup") as? ViewControllerJoinGroup
                                        
                                        self.navigationController?.pushViewController(viewControllerJoinGroup!, animated: true)
                                    })
                                    let bukaGrup = UIAlertAction(title: "Buka Grup", style: UIAlertAction.Style.default, handler: {action in
                                        let dataKirim = itemObject?["data"] as? [String: Any]
                                        let groupNameKirim = dataKirim?["groupName"] as? String
                                        let memberKirim = dataKirim?["totalMember"] as? Int
                                        let tanggalTerbuatKirim = dataKirim?["createdDate"] as? String
                                        let totalMoneyKirim = dataKirim?["totalMoney"] as? Double
                                        let wallet = dataKirim?["groupWallet"] as? [String: Any]
                                        let walletBalance = wallet?["groupWalletBalance"] as? Double
                                        print(dataKirim)
                                        
                                        self.groupDataInListGroup.setGroupName(groupName: groupNameKirim!)
                                        self.groupDataInListGroup.setTotalMember(totalMember: memberKirim!)
                                        self.groupDataInListGroup.setCreatedDate(createdDate: tanggalTerbuatKirim!)
                                        self.groupDataInListGroup.setTotalMoney(totalMoney: totalMoneyKirim!)
                                        self.groupDataInListGroup.setWallet(wallet: walletBalance!)
                                        
                                        let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
                                        
                                        self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
                                    })
                                    alert.addAction(ok)
                                    alert.addAction(bukaGrup)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            } else {
                                let alert = UIAlertController(title: "Gagal!", message: "Passcode yang kamu masukan tidak sesuai atau kamu sudah bergabung dalam grup ini.", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        
                    case .failure(let error):
                        let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print(error)
                    }
                })
    }
    
    @objc func backButtonTapped() {
        let viewControllerJoinGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerJoinGroup") as? ViewControllerJoinGroup
        
        self.navigationController?.pushViewController(viewControllerJoinGroup!, animated: true)
    }
    
    @IBAction func showPass(_ sender: Any) {
        if eyeClickPass {
            textFieldPasscode.isSecureTextEntry = false
        } else {
            textFieldPasscode.isSecureTextEntry = true
        }
        eyeClickPass = !eyeClickPass
    }
}
