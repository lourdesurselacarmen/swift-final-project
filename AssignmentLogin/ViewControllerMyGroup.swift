//
//  ViewControllerMyGroup.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerMyGroup: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewListGroup: UITableView!
    @IBOutlet weak var imageLatar: UIImageView!
    @IBOutlet weak var judulMyGroup: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var latarLogo: UIImageView!
    
    let userDefaultManager = UserDefaultManager()
    let groupDataInListGroup = GroupDataInListGroup()
    
    var arrListGroup: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        self.navigationController?.navigationBar.isHidden = true
        
        latarLogo.image = UIImage.init(named: "myGroupRemove")
        judulMyGroup.image = UIImage.init(named: "myGrup")
        imageLatar.image = UIImage.init(named: "backgroundCreate")
        imageLatar.layer.cornerRadius = 30
        imageLatar.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        tableViewListGroup.delegate = self
        tableViewListGroup.dataSource = self
        
        getMyListGroup()
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        let viewControllerHome = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerHome") as? ViewControllerHome
        
        self.navigationController?.pushViewController(viewControllerHome!, animated: true)
    }
    
    func getMyListGroup() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/search-group-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                    
                case .success:
                    let itemObject = response.value as! [String : Any]
                    let data = itemObject["data"] as! [[String: Any]]
                    self.arrListGroup = data
                    print(self.arrListGroup)
                    
                    let status = itemObject["status"] as? Int
                    let success = itemObject["success"] as? Bool
                    let message = itemObject["message"] as? String
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.tableViewListGroup.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var itemArrayListGrup = arrListGroup[indexPath.row]
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCellMyGroup
        
        cell1.viewLabel.layer.cornerRadius = 20
        
        cell1.iconGroup.image = UIImage.init(named: "iconMyGroup")
        cell1.iconNama.image = UIImage.init(named: "iconNamaGrup2")
        cell1.iconMember.image = UIImage.init(named: "iconMemberGrup2")
        cell1.iconTanggal.image = UIImage.init(named: "iconTanggalGrup2")
        cell1.iconMoney.image = UIImage.init(named: "iconTotalMoneyGrup2")
        
        let namaGrupItem = itemArrayListGrup["groupName"] as! String
        cell1.namaGrup.text = namaGrupItem
        let jumlahAnggotaItem = itemArrayListGrup["totalMember"] as! Int
        cell1.jumlahAnggota.text = "\(String(describing: jumlahAnggotaItem)) Anggota"
        let tanggalTerbuatItem = itemArrayListGrup["createdDate"] as! String
        cell1.tanggalTerbuat.text = "Since \(tanggalTerbuatItem)"
        let moneyItem = itemArrayListGrup["totalMoney"] as! Double
        let moneyItemInt = Int(moneyItem)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: moneyItemInt as NSNumber) {
            cell1.money.text = "Rp " + formattedTipAmount + ",00"
        }
        
        cell1.buttonGroupTappedCallback = {
            let token = self.userDefaultManager.getAuthToken()
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            let URL = "http://localhost:8080/api/arisan-arisan-club/search-group/\(namaGrupItem)"
            
            AF.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        //                        print(response.value)
                        let itemObjectKirim = response.value as? [String : Any]
                        let status = itemObjectKirim?["status"] as? Int
                        let success = itemObjectKirim?["success"] as? Bool
                        
                        if status ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "token")
                            
                            let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                                
                                self.navigationController?.pushViewController(viewController!, animated: true)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            if success! {
                                DispatchQueue.main.async {
                                    let dataKirim = itemObjectKirim?["data"] as? [String: Any]
                                    let groupNameKirim = dataKirim?["groupName"] as? String
                                    let memberKirim = dataKirim?["totalMember"] as? Int
                                    let tanggalTerbuatKirim = dataKirim?["createdDate"] as? String
                                    let totalMoneyKirim = dataKirim?["totalMoney"] as? Double
                                    let wallet = dataKirim?["groupWallet"] as? [String: Any]
                                    let walletBalance = wallet?["groupWalletBalance"] as? Double
                                    let passcode = dataKirim?["passcode"] as? String
                                    print(dataKirim)
                                    
                                    self.groupDataInListGroup.setGroupName(groupName: groupNameKirim!)
                                    self.groupDataInListGroup.setTotalMember(totalMember: memberKirim!)
                                    self.groupDataInListGroup.setCreatedDate(createdDate: tanggalTerbuatKirim!)
                                    self.groupDataInListGroup.setTotalMoney(totalMoney: totalMoneyKirim!)
                                    self.groupDataInListGroup.setWallet(wallet: walletBalance!)
                                    
                                    let viewControllerGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerGroup") as? ViewControllerGroup
                                    
                                    self.navigationController?.pushViewController(viewControllerGroup!, animated: true)
                                }
                            }
                        }
                        
                    case .failure(let error):
                        let alert = UIAlertController(title: "Gagal!", message: "Silahkan coba kembali.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print(error)
                    }
                })
        }
        
        return cell1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
