//
//  ViewControllerHome.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 07/12/22.
//

import UIKit
import Alamofire

class ViewControllerHome: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var imageInfoMenu: UIImageView!
    @IBOutlet weak var viewMyGroup: UIView!
    @IBOutlet weak var viewJoinGroup: UIView!
    @IBOutlet weak var viewCreateGroup: UIView!
    @IBOutlet weak var judulImage: UIImageView!
    @IBOutlet weak var imageMyGroup: UIImageView!
    @IBOutlet weak var imageJoin: UIImageView!
    @IBOutlet weak var imageCreate: UIImageView!
    @IBOutlet weak var myGroupButton: UIButton!
    @IBOutlet weak var joinGroupButton: UIButton!
    @IBOutlet weak var createGroupButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var collectionViewHome: UICollectionView!
    @IBOutlet weak var profilButton: UIButton!
    
    var arrHomeUrl = ["Home", "Philosophy", "Services", "Contact"]
    let userData = UserData()
    let userDefaultManager = UserDefaultManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        collectionViewHome.delegate = self
        collectionViewHome.dataSource = self
        
        profilButton.addTarget(self, action: #selector(profilButtonTapped), for: .touchUpInside)
        createGroupButton.addTarget(self, action: #selector(createGroupButtonTapped), for: .touchUpInside)
        createGroupButton.imageView?.contentMode = .scaleAspectFit
        
        joinGroupButton.addTarget(self, action: #selector(joinGroupButtonTapped), for: .touchUpInside)
        joinGroupButton.imageView?.contentMode = .scaleAspectFit
        
        myGroupButton.addTarget(self, action: #selector(myGroupButtonTapped), for: .touchUpInside)
        myGroupButton.imageView?.contentMode = .scaleAspectFit
        
        imageCreate.image = UIImage.init(named: "buatRemove")
        imageJoin.image = UIImage.init(named: "joinRemove")
        imageMyGroup.image = UIImage.init(named: "myGroupRemove")
        imageInfoMenu.image = UIImage.init(named: "infoMenu")
        judulImage.image = UIImage.init(named: "judulHome")
        
        viewCreateGroup.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        viewCreateGroup.layer.shadowOffset = CGSize(width: 6.0, height: 6.0)
        viewCreateGroup.layer.shadowOpacity = 1.0
        viewCreateGroup.layer.shadowRadius = 0.0
        viewCreateGroup.layer.masksToBounds = false
        viewCreateGroup.layer.cornerRadius = 4.0
        viewCreateGroup.layer.cornerRadius = 20
        viewCreateGroup.layer.borderWidth = 3
        viewCreateGroup.layer.borderColor = UIColor.lightGray.cgColor
        
        viewMyGroup.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        viewMyGroup.layer.shadowOffset = CGSize(width: 6.0, height: 6.0)
        viewMyGroup.layer.shadowOpacity = 1.0
        viewMyGroup.layer.shadowRadius = 0.0
        viewMyGroup.layer.masksToBounds = false
        viewMyGroup.layer.cornerRadius = 4.0
        viewMyGroup.layer.cornerRadius = 20
        viewMyGroup.layer.borderWidth = 3
        viewMyGroup.layer.borderColor = UIColor.lightGray.cgColor
        
        viewJoinGroup.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        viewJoinGroup.layer.shadowOffset = CGSize(width: 6.0, height: 6.0)
        viewJoinGroup.layer.shadowOpacity = 1.0
        viewJoinGroup.layer.shadowRadius = 0.0
        viewJoinGroup.layer.masksToBounds = false
        viewJoinGroup.layer.cornerRadius = 4.0
        viewJoinGroup.layer.cornerRadius = 20
        viewJoinGroup.layer.borderWidth = 3
        viewJoinGroup.layer.borderColor = UIColor.lightGray.cgColor
        
        getUserInfo()
    }
    
    func getUserInfo() {
        let token = userDefaultManager.getAuthToken()
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/api/arisan-arisan-club/current-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    print(response.value)
                    let itemObject = response.value as? [String : Any]
                    let status = itemObject?["status"] as? Int
                    let data = itemObject?["data"] as? [String: Any]
                    
                    if status ?? 0 == 401 {
                        UserDefaults.standard.removeObject(forKey: "token")
                        //logout
                        
                        let alert = UIAlertController(title: "TimeOut!", message: "Silahkan lakukan login kembali", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                            
                            self.navigationController?.pushViewController(viewController!, animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let fullName = data?["fullName"] as? String
                        self.userData.setFullname(fullName: fullName!)
                        let username = data?["username"] as? String
                        self.userData.setUsername(username: username!)
                        let dateOfBirth = data?["dateOfBirth"] as? String
                        self.userData.setBirthOfDate(birthOfDate: dateOfBirth!)
                        let phoneNumber = data?["phoneNumber"] as? String
                        self.userData.setNoTelp(phoneNumber: phoneNumber!)
                        let gender = data?["gender"] as? String
                        self.userData.setGender(gender: gender!)
                        
                        self.nameLabel.text = "Welcome, " + username!
                    }
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @objc func profilButtonTapped() {
        let viewControllerProfil = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerProfil") as? ViewControllerProfil
        
        self.navigationController?.pushViewController(viewControllerProfil!, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrHomeUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemHome = arrHomeUrl[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let imageHome = cell.viewWithTag(1) as! UIImageView
        imageHome.image = UIImage.init(named: itemHome)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 400, height: 250)
    }
    
    @objc func createGroupButtonTapped() {
        let viewControllerCreateGroup = self.storyboard?
            .instantiateViewController(withIdentifier: "ViewControllerCreateGroup") as? ViewControllerCreateGroup
        
        self.navigationController?.pushViewController(viewControllerCreateGroup!, animated: true)
    }
    
    @objc func joinGroupButtonTapped() {
        let viewControllerJoinGroup = self.storyboard?
            .instantiateViewController(withIdentifier: "ViewControllerJoinGroup") as? ViewControllerJoinGroup
        
        self.navigationController?.pushViewController(viewControllerJoinGroup!, animated: true)
    }
    
    @objc func myGroupButtonTapped() {
        let viewControllerMyGroup = self.storyboard?.instantiateViewController(withIdentifier: "ViewControllerMyGroup") as? ViewControllerMyGroup
        
        self.navigationController?.pushViewController(viewControllerMyGroup!, animated: true)
    }
}
