//
//  GroupData.swift
//  AssignmentLogin
//
//  Created by ADA-NB179 on 14/12/22.
//

import Foundation
struct GroupData {
    let groupData = UserDefaults.standard
    
    func setGroupName(groupName: String) {
        groupData.set(groupName, forKey: "groupName")
    }
    
    func getGroupName() -> String {
        return groupData.string(forKey: "groupName") ?? ""
    }
    
    func setTotalMember(totalMember: Int) {
        groupData.set(totalMember, forKey: "totalMember")
    }
    
    func getTotalMember() -> Int {
        return groupData.integer(forKey: "totalMember") 
    }
    
    func setCreatedDate(createdDate: String) {
        groupData.set(createdDate, forKey: "createdDate")
    }
    
    func getCreatedDate() -> String {
        return groupData.string(forKey: "createdDate") ?? ""
    }
}
